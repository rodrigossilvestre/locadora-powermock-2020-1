package br.ucsal.testequalidade20192.locadora;

import java.util.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.powermock.api.mockito.PowerMockito.*;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.*;
import br.ucsal.testequalidade20192.locadora.persistence.*;

/**
 * Verificar se ao locar um ve�culo dispon�vel para um cliente cadastrado, um
 * contrato de loca��o � inserido.
 * 
 * M�todo:
 * 
 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
 *
 * Observa��o1: lembre-se de mocar os m�todos necess�rios nas classes
 * ClienteDAO, VeiculoDAO e LocacaoDAO.
 * 
 * Observa��o2: lembre-se de que o m�todo locarVeiculos � um m�todo command.
 * 
 * @throws Exception
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocacaoBO.class, ClienteDAO.class, VeiculoDAO.class, LocacaoDAO.class })
public class LocacaoBOUnitarioTest {
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		//Criando Instancia de cliente e veiculo,
		Cliente cliente = new Cliente("20165874568", "Louro", "71988108888");
		Veiculo veiculo = new Veiculo("jrl-5757", 2012, new Modelo("Kombi"), 5d);
		Date data = new Date();
		
		//Criando listas de veiculos e placas
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(veiculo);
		List<String> placas = new ArrayList<>();
		placas.add(veiculo.getPlaca());
		
		//Instanciando uma nova locacao
		Locacao locacao = new Locacao(cliente, veiculos, data, 2);
		
		//Enable static mocking for all methods of a class.
		//Testando classe ClienteDAO, classe de metodo void
		PowerMockito.mockStatic(ClienteDAO.class);
		PowerMockito.when(ClienteDAO.obterPorCpf(cliente.getCpf())).thenReturn(cliente);	
		
		//Testando classe VeiculoDAO, classe de metodo void
		PowerMockito.mockStatic(VeiculoDAO.class);
		PowerMockito.when(VeiculoDAO.obterPorPlaca(veiculo.getPlaca())).thenReturn(veiculo);
		
		//Testando classe LocacaoDAO, classe de metodo void
		PowerMockito.mockStatic(LocacaoDAO.class);
		PowerMockito.whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		
		
		//String cpfCliente, List<String> placas, Date dataLocacao, Integer quantidadeDiasLocaca
 		LocacaoBO.locarVeiculos(cliente.getCpf(), placas, data, 2);

 		
		//Utilizando verify para todos os metodos usandos no teste
		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf(cliente.getCpf());
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca(veiculo.getPlaca());
		verifyStatic(LocacaoDAO.class);
		
		//Salvando locacao
		LocacaoDAO.insert(locacao);
		verifyNoMoreInteractions(LocacaoBO.class);

	}
}
